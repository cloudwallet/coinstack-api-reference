---
title: Coinstack API Reference

language_tabs:
  - shell: cURL
  - java: Java
  - javascript: NodeJs
  - meteor: Meteor

toc_footers:
  - <a href='https://dashboard.cloudwallet.io/' target='_blank'>코인스택 대시보드</a>
  - <a href='http://github.com/tripit/slate'>Documentation Powered by Slate</a>

search: true
---

# Introduction

블록체인의 세계에 오신 것을 환영합니다! 블록체인은 비트코인이 전자통화의 대표주자로 안착하기까지 핵심적인 역할을 담당해 왔습니다. 현재 블록체인 기술은 비트코인과는 별도로 최고의 데이터 안전성과 신뢰성이 요구되는 금융, 의료, 관공서 등의 분야에서 활용되거나 검토 중에 있으며, 블록체인을 기반으로 그동안 존재하지 않던 서비스들이나 참신한 아이디어로 무장한 스타트업들이 계속해서 생겨나고 있습니다.

만약 블록체인 기술을 활용할 만한 획기적인 아이디어가 있어도, 우선 블록체인과 비트코인에 대해 이해하고 공부하기 위한 많은 노력이 선행되어야 했습니다. 그리고 블록체인은 원래 범용적인 기술이 아니라 비트코인을 지탱하기 위해 만들어진 구조이므로 이를 응용하기 위해서는 추가적인 노력을 들여 기술적 장벽을 넘어야 했습니다.

코인스택은 이러한 어려움 없이 여러분의 서비스와 블록체인을 연결해 주는 징검다리 역할을 하기 위해 만들어졌습니다. 즉, 코인스택은 간편하게 블록체인 기반 서비스를 개발할 수 있도록 도와주는 플랫폼입니다. 실제로 블로코에서 서비스 중인 [클라우드스탬프](https://www.cloudstamp.io), [굿모니터](https://www.goodmonitor.kr)는 모두 코인스택 기반으로 개발되었으며, 현재 제1금융권 은행 및 대형 카드사를 비롯한 대기업, 한국거래소, 각종 스타트업들이 블록체인 기반 서비스를 제공하기 위해 코인스택을 사용하고 있습니다. 블로코가 무료로 제공하고 있는 클라우드 기반 코인스택 서비스는 과제를 수행하는 학생들, 개인 개발자들이 편리하게 이용하고 있습니다.

블록체인에 관심은 많은데 아직 잘 모르신다고요? 그렇다면 이 문서와 함께 코인스택 홈페이지에서 제공하는 블록체인 [소개](https://coinstack.zendesk.com/hc/ko/articles/222183287)와 [튜토리얼](https://coinstack.zendesk.com/hc/ko/sections/200997727) 등도 살펴보시기 바랍니다. 하지만 블록체인 전문가가 되지 않더라도 코인스택과 함께라면 얼마든지 블록체인 기반 서비스를 만들 수 있다는 사실을 잊지 마세요!

코인스택은 많은 개발자들에게 친숙한 프로그래밍 언어인 자바, 자바스크립트, HTML5 기반 SDK를 제공하며, HTML5 기반의 [웹플레이그라운드](https://playground.blocko.io/), [검색엔진](https://watch.blocko.io/), [대시보드](https://dashboard.blocko.io) 등을 통하여 편리한 개발 환경 또한 제공하고 있습니다.

지금 당장 시작해 보세요! 그리고 필요한 API나 기능이 있다면 [헬프데스크](https://coinstack.zendesk.com/hc/requests/new) 또는 [개발자 커뮤니티](https://coinstack.zendesk.com/hc/ko/community/topics)에 제안해 주시길 바랍니다.

## 문서의 구성

이 문서에서는 코인스택 개발과 관련된 모든 것을 얻을 수 있습니다. Object, API 등의 각 항목마다 우측 상단의 탭을 통해 코인스택에서 공식적으로 제공하는 모든 프로그래밍 언어별 예제를 편리하게 참조할 수 있습니다.

### 각 장의 소개

#### Quick Start Guide
코인스택 환경설정, 설치, 인증방법 등을 간략히 소개합니다.

#### Getting Started
코인스택 클라이언트의 기본적인 사용 예제를 살펴볼 수 있습니다.

#### Authentication
코인스택 클라이언트의 인증 및 권한 획득 방식과 절차를 상세히 소개합니다.

#### Coinstack and Public Bitcoin Blockchain
비트코인 블록체인을 기반으로 하는 코인스택의 기본 구성요소 및 코인스택에서 제공하는 개념과 기능을 소개합니다.

#### Coinstack Stamping
코인스택에서 제공하는 Document Stamping 기능에 대해 소개합니다.

#### Coinstack Multisig
코인스택에서 제공하는 멀티 시그니처 기능에 대해 소개합니다.

#### Coinstack Open Assets
코인스택에서 제공하는 Open Asset 프로토콜 호환 기능에 대해 소개합니다.

#### API Reference
코인스택 API에서 객체로 취급하는 데이터 모델 Object와 코인스택 API에 대한 상세한 설명 및 사용 방법, 프로그래밍 언어별 예제를 참조할 수 있습니다.

## 프로그래밍 언어 지원
코인스택의 모든 기능은 REST API로 제공되며, 아래 환경에서 SDK를 설치하여 사용할 수 있습니다.

플랫폼 | 다운로드
--------- | -----------
Java | <a href='http://mvnrepository.com/artifact/io.blocko/coinstack'>Maven Repository</a>
Node.js | <a href='https://www.npmjs.com/package/coinstack-sdk-js'>NPM JS</a>
Meteor | <a href='https://atmospherejs.com/shepelt/coinstack'>ATMOSPHERE</a>
HTML5 | <a href='https://cdnjs.coinstacks.io/coinstack-1.1.10.min.js'>Coinstack CDN</a>

## Copyright
코인스택과 본 매뉴얼에 대한 모든 저작권은 ㈜블로코에 있습니다.

## Company Info.
주식회사 블로코 (www.blocko.io)

경기 성남시 분당구 성남대로 331번길 8(정자동) 킨스타워 902호

## Contact
* 기술지원: 031-8016-6253
* 팩스: 050-8054-6253
* E-mail: support@blocko.io

## 매뉴얼 정보
* 소프트웨어 버전: 코인스택 3.0
* 매뉴얼 버전: 1.0.0
* 발행일: 2016-12-01


# Quick Start Guide

블로코에서는 웹브라우저 상에서 설치 없이 빠르게 코인스택 SDK를 경험해 볼 수 있는 [웹플레이그라운드](https://playground.blocko.io/)를 제공하고 있습니다. 본 문서에서는 플레이그라운드를 통해 비트코인 블록체인의 상태를 조회하고, 주소 생성 및 잔고를 조회하는 방법을 소개합니다. 각 과정에 대한 상세한 설명은 Coinstack Documentation의 다른 챕터들을 참조하시기 바랍니다. 코인스택을 활용하여 멋진 서비스를 개발해 보시기 바랍니다.

## 웹플레이그라운드 접속
크롬이나 인터넷익스플로러 등 웹브라우저를 통해 코인스택 웹플레이그라운드([https://playground.blocko.io/](https://playground.blocko.io/))에 접속합니다. 코드들은 웹페이지 상의 콘솔창에 입력하거나, 웹페이지 하단의 예제코드 항에 존재하는 RUN 버튼을 누르면 실행 가능합니다.

## 클라이언트 객체 생성
테스트용 API Access Key와 Secret Key를 사용하여 CoinStack 객체를 생성합니다. 실 사용을 위한 코인스택 API KEY 발급은 4장 Authentication을 참고 바랍니다.

```javascript
var client = new CoinStack('c7dbfacbdf1510889b38c01b8440b1', '10e88e9904f29c98356fd2d12b26de');
console.log(client);
```
>결과값은 JSON으로 리턴됩니다.
>```json
{  
   "apiKey":"c7dbfacbdf1510889b38c01b8440b1",
   "secretKey":"10e88e9904f29c98356fd2d12b26de",
   "protocol":"https://",
   "endpoint":"mainnet.cloudwallet.io",
   "isBrowser":true
}
```

## 최신 블록체인 상태 정보 조회
블록체인의 상태를 조회하여 최신 블록의 블록번호와 해시값을 출력해 봅니다.
```javascript
client.getBlockchainStatus(function(err, status) {
	console.log(status);
});
```
>결과값은 JSON으로 리턴됩니다.
>```json
{  
   "best_block_hash":"0000000000000000002f28ea6b32950216406511722839dd47752f3b06e1d025",
   "best_height":433116
}
```

## 새 비공개키 생성
임의의 비공개키를 생성해 봅니다. 본 예제의 결과에 나온 Secret Key는 테스트 용이며, **추후 본인의 개인 계정을 생성하고 발급받은 Secret Key는 절대 타인에게 공개되어서는 안됩니다.**
```javascript
var privateKey = CoinStack.ECKey.createKey();
console.log(privateKey);
```
>결과값은 문자열로 리턴됩니다.
>```
L3nkFqH4n9xoYFvEmEyg54utGogNdz1WA4fqRohMJ8VgkXpRvGs1
```

## 비공개키에서 주소 생성
지갑 주소를 생성해 봅니다. 이 주소는 외부에 공개 가능하며, 이 주소를 통해 비트코인을 전송받을 수 있습니다.
```javascript
var address = CoinStack.ECKey.deriveAddress(privateKey);
console.log(address);
```
>결과값은 문자열로 리턴됩니다.
>```
1MVMj4Gr9e7U5D4ZLignuNLpv2cyKiHa2x
```

## 주소 잔고 조회
현재 주소의 비트코인 잔액을 조회하고 사토시 단위로 출력해 봅니다. (1 사토시 = 0.00000001 비트코인, 1 비트코인 = 1억 사토시) 본 예제의 주소는 새로 생성한 주소이므로 잔액이 0 비트코인(BTC)임을 확인할 수 있습니다.

```javascript
client.getBalance(address,
function(err, balance) {
	console.log(CoinStack.Math.toBitcoin(balance) + ' BTC');
});

```
>결과값은 문자열로 리턴됩니다.
>```
0 BTC
```

# Getting Started

본 문서에서는 코인스택을 사용하기 위한 클라이언트의 초기 환경 설정, SDK 설치, 구동 테스트 등을 상세히 소개합니다. 무료로 공개된 코인스택 서버는 클라우드 상에서 운영되고 있습니다. 만약 구축형(On-Premise)으로 별도의 설치가 필요한 경우 블로코의 기술지원이나 영업 채널로 문의 바랍니다.


## API KEY 발급
코인스택을 사용하기 위해서는 우선 API KEY가 준비되어야 합니다. [코인스택 대시보드](https://dashboard.blocko.io/)에서 계정을 생성한 후 인증키를 발급받을 수 있습니다. 대시보드의 [키 발급] 화면에서 [새로운 키 발급] 버튼을 누르고 [API Key]를 선택하면 Access Key와 Secret Key가 생성됩니다. **Secret Key는 대시보드에서 다시 확인할 수 없으므로** 키를 발급받을 때 화면에 표시된 Secret Key를 반드시 다른 곳에 저장해 두어야 합니다.

코인스택 인증 방식에 대한 자세한 사항은 4장 Authentication을 참고 바랍니다.


## Java

### 1. 자바 SDK 설치
코인스택 자바 SDK를 사용하려면 JDK 1.6 또는 상위 버전의 JDK가 필요합니다. [Oracle Java SE Downloads](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 페이지에서 최신 버전의 JDK를 다운로드 받을 수 있습니다. 자세한 다운로드 및 설치방법은 [공식 페이지](https://www.java.com/ko/download/help/download_options.xml)를 참조하시기 바랍니다. 개발 편의를 위하여 환경변수(JAVA_HOME, PATH)를 설정 할 경우 [환경변수 설정 페이지](https://www.java.com/ko/download/help/path.xml)를 참조하시기 바랍니다.

### 2. 코인스택 SDK 설치
코인스택 자바 SDK를 설치할 때는 Maven 같은 패키지 매니저를 추천합니다. 패키지 매니저를 사용하지 않고 직접 jar 파일을 추가해서 사용하려면 [Maven 저장소](http://central.maven.org/maven2/io/blocko/coinstack/)에서 최신 버전의 SDK를 다운로드 받을 수 있습니다.

Maven 환경에서는 `pom.xml` 파일에 아래 내용을 추가해 줍니다.
```xml
<dependency>
    <groupId>io.blocko</groupId>
    <artifactId>coinstack</artifactId>
    <version>3.0.23</version>
</dependency>
```

### 3. 클라이언트 생성과 종료
앞서 발급받은 API KEY를 가지고 CoinStackClient를 생성합니다. CredentialsProvider는 코인스택 API KEY와 접속할 서버를 가리키는 EndPoint를 관리합니다. Endpoint에는 SDK를 통해 접근하고자 하는 네트워크 주소를 입력하는데, 코인스택이 제공하는 클라우드 서비스를 통해 비트코인 네트워크에 접속하고자 할 경우 코인스택 메인넷([https://mainnet.cloudwallet.io](https://mainnet.cloudwallet.io))을 사용합니다. 만약 비트코인 테스트 네트워크에 접속하려면 테스트넷([https://regtestnet.cloudwallet.io](https://regtestnet.cloudwallet.io))을 사용합니다. 설치형인 경우 Endpoint 인터페이스를 구현하여 해당 서버 접속 정보를 입력한 후 사용합니다.

```java
//import io.blocko.coinstack.*
//import io.blocko.coinstack.model.*

CoinStackClient client = new CoinStackClient(new CredentialsProvider() {
  @Override
  public String getAccessKey() {
    return "YOUR_COINSTACK_ACCESS_KEY";
  }
  @Override
  public String getSecretKey() {
    return "YOUR_COINSTACK_SECRET_KEY";
  }
}, Endpoint.MAINNET);
```

CoinStackClient의 사용을 마친 후에는 명시적으로 종료를 선언하여 리소스를 반환합니다.
```java
client.close()
```

### 4. 구동 테스트
설치한 코인스택 SDK가 정상적으로 동작하는지 확인하기 위해 블록체인의 상태 정보를 조회해 봅니다. 이를 위해서는 getBlockchainStatus 메소드를 이용합니다. 상태 정보 객체에 getBestHeight 메소드를 사용하면 최신 블록 번호를 알 수 있고, getBestBlockHash 메소드는 최신 블록의 해시를 반환합니다. 블록의 해시는 블록의 정보를 조회할 때 ID로 사용할 수 있습니다.
```java
BlockchainStatus status = client.getBlockchainStatus();
System.out.println("bestHeight: "+ status.getBestHeight());
System.out.println("bestBlockHash: "+ status.getBestBlockHash());
```

## Node.js
### 1. 코인스택 패키지 설치
npm (Node Packaged Modules) 패키지 매니저를 사용하여 CoinStack SDK를 설치할 수 있습니다.
```shell
npm install coinstack-sdk-js
```
### 2. 클라이언트 생성과 종료
앞서 발급받은 API KEY를 사용하여 CoinStack 객체를 생성합니다.
```javascript
var CoinStack = require('coinstack-sdk-js')

var accessKey = "YOUR_COINSTACK_ACCESS_KEY";
var secretKey = "YOUR_COINSTACK_SECRET_KEY";
var client = new CoinStack(accessKey, secretKey);
```
### 3. 구동 테스트
다음과 같이 블록체인 상태 정보를 조회해 봅니다.
```javascript
client.getBlockchainStatus(function(err, status) {
    console.log(status.best_height);
    console.log(status.best_block_hash);
});
```


## Meteor
### 1. 코인스택 패키지 설치
atmosphere 패키지 매니저를 이용하여 CoinStack SDK를 설치합니다.
```shell
meteor add shepelt:coinstack
```
### 2. 클라이언트 생성과 종료
앞서 발급받은 API KEY를 가지고 CoinStack 객체를 생성합니다.
```javascript
var accessKey = "YOUR_COINSTACK_ACCESS_KEY";
var secretKey = "YOUR_COINSTACK_SECRET_KEY";
var client = new CoinStack(accessKey, secretKey);
```
### 3. 구동 테스트
다음과 같이 블록체인의 상태 정보를 조회해 봅니다.
```javascript
var status = client.getBlockchainStatusSync();
console.log(status.best_height);
console.log(status.best_block_hash);
```

# Authentication

## 1. 개요
코인스택 API를 사용하기 위해서는 코인스택 서버에 자격 증명(credential)을 제공해야 합니다. 자격 증명은 사용자의 요청이 변조되지 않았음을 보장하는 역할과, 각 사용자의 요청을 구별하는 역할을 수행합니다. 자격 증명 절차는 다음과 같습니다.

1.  <a href='https://dashboard.blocko.io'>대시보드</a>에서 API 키 발급 받기
2.  직접 REST 호출을 원하는 경우 토큰 발급
3.  자신의 프로그램이나 환경 변수에 설정을 원할 경우 API키 발급
4.  발급 받은 자격증명을 아래의 설명에 따라 설정하여 코인스택 API 호출

## 2. 자격 증명 방식
자격 증명은 아래 두 가지 방식으로 서버에 전달됩니다.

### 2.1. 토큰
REST API를 직접 사용하는 경우에는 토큰을 HTTP 요청의 헤더에 포함하여 보내야 합니다. 이 경우 토큰이 평문으로 전달되기 때문에, TLS 같은 안전한 프로토콜을 권장합니다.
### 2.2. 서명
SDK를 사용하여 API를 호출하는 경우 API 키와 비밀키로 request에 대한 전자 서명을 HMAC-SHA256 방식으로 생성하여 전달합니다.

## 3. API 키 발급 및 설정

### 3.1. API 키 발급
위에서 설명한 바와 같이, 코인스택 API를 사용하기 위해서는 먼저 자격 증명이 필요합니다. <a href='https://dashboard.blocko.io'>대시보드</a>에서 API 키 또는 API 토큰을 발급받을 수 있습니다. Java와 Node, Meteor에서는 Access Key와 Secret Key를 사용하며, curl 등을 통해 직접 REST API를 호출하는 경우에는 Access Token을 사용합니다.

### 3.2. API 키 설정
#### 3.2.1. 환경 변수 사용
API 키 또는 API 토큰을 환경 변수에 설정하면 코드 내에 이 값들을 직접 입력하는 일을 피할 수 있습니다. 발급받은 Access Key를 COINSTACK_ACCESS_KEY_ID라는 환경 변수에, Secret Key를 COINSTACK_SECRET_ACCESS_KEY에 각각 할당합니다. 환경 변수를 설정하는 자세한 방법은 [환경 변수 설정 페이지](https://www.java.com/ko/download/help/path.xml)를 참조 바랍니다.

환경 변수를 설정하면, 다음과 같이 기본 생성자를 이용해 CoinStackClient 객체를 생성할 수 있습니다.

##### Java
```java
import io.blocko.coinstack.*

CoinStackClient client = new CoinStackClient();
```

##### Node.js
```javascript
var CoinStack = require('coinstack-sdk-js')
var client = new CoinStack();
```

##### Meteor
```javascript
export COINSTACK_ACCESS_KEY="YOUR_COINSTACK_ACCESS_KEY";
export COINSTACK_SECRET_KEY="YOUR_COINSTACK_SECRET_KEY";
var client = new CoinStack();
```

#### 3.2.2. 코드 내에 직접 입력
##### REST API
curl을 사용하여 REST API를 통해 키를 직접 입력, 호출하는 방법은 다음과 같습니다.
```shell
curl https://mainnet.cloudwallet.io/ \
    -H "apiKey: YOUR_API_TOKEN_KEY"
```

##### Java
다음과 같이 객체 생성자에 직접 키를 입력할 수도 있습니다.
```java
//import io.blocko.coinstack.*
//import io.blocko.coinstack.model.*

CoinStackClient client = new CoinStackClient(new CredentialsProvider() {
			@Override
			public String getAccessKey() {
				return "발급받은 access key";
			}

			@Override
			public String getSecretKey() {
				return "발급받은 secret key";
			}
		}, Endpoint.MAINNET);
```

##### Node.js

```javascript
var accessKey = "YOUR_COINSTACK_ACCESS_KEY";
var secretKey = "YOUR_COINSTACK_SECRET_KEY";
var client = new CoinStack(accessKey, secretKey);
```

##### Meteor
```javascript
var accessKey = "YOUR_COINSTACK_ACCESS_KEY";
var secretKey = "YOUR_COINSTACK_SECRET_KEY";
var client = new CoinStack(accessKey, secretKey);
```

# Coinstack And Public Bitcoin Blockchain

## 1. Blockchain과 Block
블록체인은 분산 데이터베이스 기술의 하나로, 운영자나 사용자라 할지라도 저장된 데이터를 임의로 조작하지 못하도록 고안되었습니다. 일반적인 데이터베이스와는 달리 블록체인은 자발적으로 참여하는 수 십, 수 백, 또는 수 천 개의 노드들에 의하여 운영되며, 이를 제어하기 위한 중앙 관리자가 존재하지 않습니다. 블록체인은 거래(transaction)들이 저장되는 거대한 분산 장부로서, 거래는 블록(block)에 담겨서 노드간에 공유되고 이 블록들이 사슬처럼 연결된 구조가 바로 블록체인입니다.

### 1.1. Blockchain 상태 조회
앞서 Getting Started 장에서 사용한 getBestBlockHash 메소드를 통해 블록 체인의 최신 상태 정보를 조회할 수 있습니다.

#### JAVA
```java
String[] blockchainStatus = client.getBlockchainStatus();
```
#### REST API
```shell
YOUR_API_TOKEN_KEY="YOUR_API_TOKEN_KEY"

curl https://mainnet.cloudwallet.io/blockchain \
    -H "apiKey: $YOUR_API_TOKEN_KEY"
```
> 결과값은 JSON으로 반환됩니다.
```json
{
  "best_block_hash": "000000000000000007e5724849ad23a76aa9db734e9671b2b08c075ab017fb6c",
  "best_height": 370353
}
```

#### Node.js
```javascript
client.getBlockchainStatus(function(err, status) {
    console.log(status.best_block_hash, status.best_height);
});
```

#### Meteor
```javascript
// server side
var status = client.getBlockchainStatusSync()
console.log(status.best_block_hash, best_height)

// client side
client.getBlockchainStatus(function(err, status) {
    console.log(status.best_block_hash, status.best_height);
});
```

### 1.2. Block 조회
특정 블록의 정보를 얻기 위해서는 블록의 ID인 블록 해시 값이 필요합니다.  CoinStackClient 객체의 getBlock 메소드를 호출하면 입력한 블록 ID에 해당하는 블록의 정보가 담겨 있는 Block 객체를 반환합니다. Block 객체의 getBlockId, getParentId, getHeight 메소드를 통해 각각 블록 해시, 이전 블록의 해시, 블록 번호를 확인할 수 있습니다.

#### JAVA
```java
Block block = client.getBlock("BLOCK_ID");
System.out.println("blockId: " + block.getBlockId());
System.out.println("parentId: " + block.getParentId());
System.out.println("height: " + block.getHeight());
System.out.println("time: " + block.getBlockConfirmationTime());
```

> 수행 결과는 다음과 같습니다.
>  ```
blockId: 0000000000000000017363cc0f6562f4e4552b94032c49ce37f5fb1309062fc6
parentId: 00000000000000000428ec876f074bc7b34824bb11f8b00ec764622b3f54861d
height: 433254
time: Fri Oct 07 15:25:34 KST 2016
```

#### REST API
```shell
BLOCK_ID = "BLOCK_ID"

curl https://mainnet.cloudwallet.io/blocks/$BLOCK_ID \
    -H "apiKey: $YOUR_API_TOKEN_KEY"
```
> 수행 결과는 다음과 같습니다.
>  ```
{
  "block_hash": "0000000000000000035bce787da99a937602b7b47367c1b4b026149eb699df72",
  "height": 433254,
  "confirmation_time": "2016-10-07T06:25:34Z",
  "parent": "00000000000000000428ec876f074bc7b34824bb11f8b00ec764622b3f54861d",
  "transaction_list": [
    "d855dea8da20fad1b6f0d1654f4fea0d933809f315216e4ea60b984ee947faed",
    "b613a39216eab6af00f29ef584bd4dd057497d4b8410a0dc97651d1a18b44878",
    "...."
  ]
}
```

#### Node.js
```javascript
client.getBlock("BLOCK_ID", function(err, result) {
    console.log('result', result);
});
```

#### Meteor
```javascript
// server
var result = client.getBlock("BLOCK_ID") {
    console.log(result.block_hash, result.height);
});

// client
client.getBlock("BLOCK_ID", function(err, result) {
    console.log('result', result);
});
```

## 2. Addresses
블록체인 서비스나 비트코인을 사용하기 위해서는 주소를 생성해야 합니다. 주소는 비트코인 네트워크 상에서 자신의 신원을 나타냅니다. 아래 설명할 개인키와 달리, 비트코인 주소는 다른 사람들과 공유하여 자신의 주소로 비트코인을 받기도 하고 주소에 담겨있는 비트코인을 다른 주소로 보내기도 합니다. 블록체인 응용 서비스에서는 좀 더 다양하게 주소를 활용하기도 하는데, TSA(Time Stamp Authority)라 불리는 문서 증명의 경우 이 주소를 문서 hash값을 담는 주소로 활용합니다. 개인 인증으로 활용할 경우는 이 주소를 인증서의 보관 장소로 활용할 수 있습니다. 비트코인 주소는 ECDSA(Eliptic Curve Digital Signature Algorithm)를 기반으로 하고 있으며, 그 주소 자체가 암호화 성격을 가지고 있으므로 다양한 분야에 적용할 수 있습니다.

### 2.1. Address 생성

비트코인 주소를 생성하기 위해서는 ECDSA 알고리즘 기반의
공개키와 개인키를 먼저 생성해야 합니다. 생성 순서는 다음과 같습니다.

> Random  Number Generation -> 개인키 생성 -> 공개키 생성 -> 비트코인 주소 생성

코인스택에서는 사용자가 위 과정을 이해하고 직접 수행할 필요 없이 손쉽게 주소를 생성할 수 있는 API를 제공합니다.

#### 2.1.1. 개인키 생성

1에서 2^256 사이의 숫자 중 무작위로 정수 하나를 고르게 되는데 이것이 바로 개인키가 됩니다.
개인키는 보통 Base58Check으로 인코딩하여 사용하는데, 이것을 비트코인에서는 Private Key WIF
(wallet import format)라고 부릅니다. WIF 형으로 변화하면 그 길이가 짧아지고 혼동 하기 쉬운 문자열
(1 또는 i)이 제거되어 편리한 형태가 됩니다.

**개인키는 유출되지 않도록 주의하여야 합니다.** 개인키가 유출되면 내 지갑의 비트코인을 도난당할 수 있습니다.

다음은 코인스택이 제공하는 개인키 생성 메소드입니다. 이처럼 코인스택을 사용하면 임의의 새로운 개인키를
빠르고 간편하게 생성할 수 있습니다.

##### Java
```java
// create a new private key
String newPrivateKeyWIF = ECKey.createNewPrivateKey();
System.out.println("private key: " + newPrivateKeyWIF);
```
> 결과값은 문자열로 반환됩니다.
```
private key: L3nWhxhA68enYiXa2D1r4dj6bGDqXECEDoTgsgd4smBf8xrtnFED
```

#### 2.1.2. 공개키 및 비트코인 주소 생성

공개키는 ECDSA 함수 중 하나인 secp256k1에 개인키를 입력값으로 한 결과 좌표 (x,y)로 표현됩니다.
우리가 사용하는 공개키의 형식은 이 좌표값 x와 y를 연결한 후 접두어를 붙인 형태입니다.

##### Java
```java
// derive a public key
String newPublicKey = Hex.encodeHexString(ECKey.derivePubKey(newPrivateKeyWIF));
System.out.println("public key: " + newPublicKey);
```
> 결과값은 문자열로 반환됩니다.
```
public key: 04ed0f040740a2d2e60a9910b3bd94c92217d387a6bec8e9c78e246b78160fa64e9a4f1a82ec07ed7a9070cb26e32c3f770d31783a8f8456113c2d76f95d1166c2
```

비트코인 주소는 이 공개키를 RIPEMD160(SHA256(K)) 함수로 다시 인코딩하여 생성합니다. 다음은 코인스택을 사용하여 개인키에서 비트코인 주소를 생성하는 예시입니다.

##### Java
```java
// derive an address
String your_wallet_address = ECKey.deriveAddress(newPrivateKeyWIF);
System.out.println("address: " + your_wallet_address);
```
> 결과값은 문자열로 반환됩니다.
```
address: 1MG4Y8JwqrMyNHpot9xvj62v12aPwuBB6W
```

### 2.4. Address Balance 조회
주소는 비트코인을 담고 있거나 OP_RETURN data를 담고 있습니다. 흩어져 있는 UTXO(Unspent output)의
비트코인 수량을 합산하여 보여주는 기능은 다음과 같습니다. (e.g. 0.0001 BTC = 10000 satoshi)

##### Java
```java
// get a remaining balance
long balance = client.getBalance(your_wallet_address);
System.out.println("balance: " + balance);
```
> 결과값은 사토시 단위이며 Long 타입으로 반환됩니다.
```json
balance: 566368820
```

##### REST API
```shell
YOUR_WALLET_ADDRESS="YOUR_WALLET_ADDRESS"

curl https://mainnet.cloudwallet.io/addresses/$YOUR_WALLET_ADDRESS/balance \
    -H "apiKey: $YOUR_API_TOKEN_KEY"
```
> 결과값은 사토시 단위이며 JSON 형식으로 반환됩니다.
```json
{"balance":566368820}
```

##### Node.js
```javascript
coinstackclient.getBalance("YOUR_WALLET_ADDRESS", function(err, balance) {
    console.log(balance);
});
```

##### Meteor
```javascript
// server
var balance = coinstackclient.getBalanceSync("YOUR_WALLET_ADDRESS");
console.log(balance);

// client
coinstackclient.getBalance("YOUR_BLOCKCHAIN_ADDRESS", function(err, balance) {
    console.log(balance);
});
```

### 2.5. Address별 Transaction 조회
앞서 살펴본 바와 같이 주소는 비트코인을 담고 있거나 OP_RETURN 데이터를 담고 있습니다. 다음은 특정 주소에서 일어난 비트코인 거래 내역 또는 OP_RETURN 데이터의 내용을 조회하는 기능입니다.




##### Java
```java
// print all transactions of a given wallet address
String[] transactionIds = client.getTransactions(your_wallet_address);
System.out.println("transactions");
for (String txId : transactionIds) {
  System.out.println("txIds[]: " + txId);
}
```
> 결과 값은 문자열의 배열로 반환됩니다.
```
transactions
4fe42d98bdede1eedb504c48a7670b18b1d3691ae140d313e529ab92d53c7aa0
ed7b57daba7621e726eab433823faa107cc20fdfe6c53c322288cb4161d850c1
```

##### REST API
```shell
curl https://mainnet.cloudwallet.io/addresses/YOUR_WALLET_ADDRESS/history \
 -H "apiKey: $YOUR_API_TOKEN_KEY"
```
> 결과 값은 JSON 형식으로 반환됩니다.
```json
["4fe42d98bdede1eedb504c48a7670b18b1d3691ae140d313e529ab92d53c7aa0",
"ed7b57daba7621e726eab433823faa107cc20fdfe6c53c322288cb4161d850c1",
...
]
```

##### Node.js
```javascript
coinstackclient.getTransactions("YOUR_WALLET_ADDRESS", function(err, result) {
    console.log(result);
});
```

##### Meteor
```javascript
// server
var result = coinstackclient.getTransactionsSync("YOUR_WALLET_ADDRESS");
console.log(result);


// client
coinstackclient.getTransactions("YOUR_WALLET_ADDRESS", function(err, result) {
    console.log(result);
});
```


### 2.6. Address별 unspent outputs 조회
블록체인은 주소 별로 잔고를 관리하고 있는 것이 아니라 단순히 거래 내역을 기록하고 있을 뿐입니다. 자신의 주소에 해당하는 Transaction output은 두 가지 종류로 나눌 수 있는데, 한 가지는 Spent ouput, 다른 한 가지는 일반적으로 UTXO라 불리는 Unspent output입니다. 새로운 거래를 만들기 위해서는 이 UTXO를 input으로 입력해야 하는데, 코인스택에서는 이를 위해 특정 주소와 연관된 UTXO들을 조회하는 기능을 제공합니다.

##### Java
```java
//print all utxos
Output[] outputs = client.getUnspentOutputs(your_wallet_address);
System.out.println("unspent outputs");
for (Output utxo: outputs) {
    System.out.println(utxo.getValue());
}
```
> 결과값은 Output 객체의 배열로 반환됩니다. 예제에서는 Output이 가진 잔고를 출력합니다.
```
unspent outputs
7259033307
299990000
961900000
```

##### REST API
```shell
curl https://mainnet.cloudwallet.io/addresses/YOUR_WALLET_ADDRESS/unspentoutputs \
 -H "apiKey: $YOUR_API_TOKEN_KEY"
```
> 결과값은 JSON 형식으로 반환됩니다.

```json
[
 {
 "transaction_hash": "f2020daaf62e03deb2c6f8a94988a6cca3d66273dc6fa2169e88f02b288520ea",
 "index": 1,
 "value": "7259033307",
 "script": "76a914a13ca67f70cc4afefd4057f87d1e433dab05a20788ac",
 "confirmations": 2509
 },
 {
 "transaction_hash": "9b30059da6517a08034c0faf3bd347a7a24189285fa80ede850ecc552d165620",
 "index": 1,
 "value": "299990000",
 "script": "76a914a13ca67f70cc4afefd4057f87d1e433dab05a20788ac",
 "confirmations": 99
 },
 {
 "transaction_hash": "9ac9ab9285ebb5107bfc2e086d35c3ebefd9a899722039fac667e6ece3b20c90",
 "index": 1,
 "value": "961900000",
 "script": "76a914a13ca67f70cc4afefd4057f87d1e433dab05a20788ac",
 "confirmations": 91
 }
]
```

##### Node.js
```javascript
coinStackClient.getUnspentOutputs("YOUR_WALLET_ADDRESS", function(err, result) {
    console.log('result', result);
});
```

##### Meteor
```javascript
// server
var result = coinstackclient.getUnspentOutputsSync("YOUR_WALLET_ADDRESS");
console.log('result', result);

// client
coinstackclient.getUnspentOutputs("YOUR_WALLET_ADDRESS", function(err, result) {
    console.log('result', result);
});

```



## 3. Transactions

### 3.1. 트랜잭션 생성
트랜잭션을 이용하여 특정 블록체인 주소로 비트코인을 전송하거나, 데이터를 저장할 수 있습니다.

#### 3.1.1. Unspent Output 조회
새로운 트랜잭션을 만들기 위해서는 일단 input으로 사용할 unspent output을 조회하여 사용 가능한 잔고를 확인해야 합니다. SDK에 포함된 transaction builder를 이용하면 트랜잭션 생성시 자동으로 unspent output을 조회하여 최적의 unspent output을 선택해 줍니다.


#### 3.1.2. 트랜잭션 생성 및 서명
##### 3.1.1.1. 일반 트랜잭션
일반적으로 트랜잭션은 내 주소의 잔여 비트코인을 타 주소로 전송하기 위해 사용됩니다. 이 경우 비트코인을 전송받을 주소와 전송할 금액, 사용할 unspent output을 지정해야 합니다. (unspent output은 SDK가 자동적으로 지정해 줍니다.) 그리고 개인키로 서명하여 지정한 unspent output의 소유권을 증명합니다.

##### java
```java
// create a target address to send
String toPrivateKeyWIF = ECKey.createNewPrivateKey();
String toAddress = ECKey.deriveAddress(toPrivateKeyWIF);

// create a transaction
long amount = io.blocko.coinstack.Math.convertToSatoshi("0.0002");
long fee = io.blocko.coinstack.Math.convertToSatoshi("0.0001");

TransactionBuilder builder = new TransactionBuilder();
builder.addOutput(toAddress, amount);
builder.setFee(fee);

// sign the transaction using the private key
String signedTx = client.createSignedTransaction(builder, "YOUR_PRIVATE_KEY");
System.out.println(signedTx);
```
> 결과값은 문자열로 반환됩니다.

```
01000000 01 be66e10da854e7aea9338c1f91cd489768d1d6d7189f586d7a3613f2a24d5396 00000000 8c 49 3046022100cf4d7571dd47a4d47f5cb767d54d6702530a3555726b27b6ac56117f5e7808fe0221008cbb42233bb04d7f28a715cf7c938e238afde90207e9d103dd9018e12cb7180e 01 41 042daa93315eebbe2cb9b5c3505df4c6fb6caca8b756786098567550d4820c09db988fe9997d049d687292f815ccd6e7fb5c1b1a91137999818d17c73d0f80aef9 ffffffff 01 23ce010000000000 19 76 a9 14 a2fd2e039a86dbcf0e1a664729e09e8007f89510 88 ac 00000000
```

##### Node.js
```javascript
var txBuilder = coinstackclient.createTransactionBuilder();
txBuilder.addOutput("TO_ADDRESS", coinstackclient.Math.toSatoshi("0.0001"));
txBuilder.setInput("YOUR_WALLET_ADDRESS");

txBuilder.buildTransaction(function(err, tx) {
    tx.sign("YOUR_PRIVATE_KEY")
    var rawTx = tx.serialize()
    console.log(rawTx)
});
```

##### Meteor
```javascript
// server
var txBuilder = coinstackclient.createTransactionBuilder();
txBuilder.addOutput("TO_ADDRESS", coinstackclient.Math.toSatoshi("0.0001"));
txBuilder.setInput("YOUR_WALLET_ADDRESS");
var tx = coinstackclient.buildTransactionSync(txBuilder);
tx.sign("YOUR_PRIVATE_KEY");
var rawTx = tx.serialize();
console.log(rawTx)

// client
var txBuilder = coinstackclient.createTransactionBuilder();
txBuilder.addOutput("TO_ADDRESS", coinstackclient.Math.toSatoshi("0.0001"));
txBuilder.setInput("YOUR_WALLET_ADDRESS");
txBuilder.buildTransaction(function(err, tx) {
    tx.sign("YOUR_PRIVATE_KEY");
    var rawTx = tx.serialize();
    console.log(rawTx)
});
```

비트코인 클라이언트에 따라 다르지만, 너무 적은 액수의 비트코인을 전송하는 트랜잭션은 dust threshold라는 정책에 의해 전송이 거부당할 수 있습니다.

Coinstack SDK의 transaction builder를 사용하는 경우 너무 적은 액수의 비트코인을 송금하려고 하면 SDK에서 오류가 발생합니다. 현재 코인스택 SDK에 기본으로 설정된 최소 전송 가능 액수는 5460 사토시이며, allowDustyOutput(true) 함수를 사용하면 이 설정을 비활성화하여 더 적은 양의 비트코인도 전송할 수 있습니다.

그러나 코인스택 SDK와는 별개로 각 블록체인 서버에 상이한 dust threshold 정책이 존재할 수 있으므로 접속하시는 서버의 정책 또한 확인하여 사용하시기 바랍니다. 현재 코인스택에서 제공하는 비트코인 클라우드 서비스의 dust threshold는 546 사토시이며, 구축형 모델에서는 이 값을 변경할 수 있습니다.

```java
builder.allowDustyOutput(true);
```

##### 3.1.1.2. Data 트랜잭션

비트코인을 전송하는 대신 OP_RETURN 이라는 output script를 사용하여 트랜잭션에 데이터를 저장할 수 있습니다.
코인스택 SDK에 포함된 transaction builder에서 제공하는 인터페이스를 사용하면 블록체인에 데이터를 손쉽게 저장할 수 있습니다.
코인스택에서 제공하는 stamping 및 Open Assets 관련 기능은 바로 이 OP_RETURN을 이용하여 구현되었습니다.

```java
builder.setData("DATA_AT_OP_RETURN".getBytes());
```

### 3.2. 트랜잭션 전송
서명된 트랜잭션은 이제 네트워크에 전송될 수 있습니다. 만약 전송 과정에서 문제가 발생하거나, 트랜잭션 자체에 오류가 있으면 관련된 오류 코드로 원인을 확인할 수 있습니다. 하지만 트랜잭션 자체에 오류가 없는 경우에도 블록체인 네트워크의 특성상 일시적으로 트랜잭션이 거부되는 경우가 있습니다. 이 때는 네트워크에 성공적으로 전파될 때까지 재전송하면 됩니다.

##### Java
```java
// send the signed transaction
client.sendTransaction(signedTx);
```

##### Node.js
```javascript
txBuilder.buildTransaction(function(err, tx) {
    tx.sign("YOUR_PRIVATE_KEY")
    var rawTx = tx.serialize()
    // send transaction
    coinstackclient.sendTransaction(rawTx, function(err) {
        if (null != err) {
            console.log("failed to send tx");
        }
    );
});
```

##### Meteor

```javascript
// server
try {
    // send tx
    coinstackclient.sendTransactionSync(rawTx);
} catch (e) {
    console.log("failed to send tx");
}

// client
txBuilder.buildTransaction(function(err, tx) {
    tx.sign("YOUR_PRIVATE_KEY");
    var rawTx = tx.serialize();
    // send tx
    coinstackclient.sendTransaction(rawTx, function(err) {
        if (null != err) {
            console.log("failed to send tx");
        }
    );
});
```

### 3.3. 트랜잭션 조회
생성한 트랜잭션의 ID를 조회하는 방법은 다음과 같습니다.

##### Java
```java
String transactionId = TransactionUtil.getTransactionHash(signedTx);
```
트랜잭션이 네트워크에 성공적으로 반영된 경우, 다음과 같이 해당 ID로 트랜잭션 정보를 조회할 수 있습니다.  

##### Java
```java
// print transaction
Transaction tx = client.getTransaction("YOUR_TRANSACTION_ID");
System.out.println(tx.getConfirmationTime())
```

> 결과 값은 Transaction 객체로 반환됩니다. 예제에서는 Transaction이 확정된 시간을 출력합니다.
```
Mon Oct 10 18:13:34 KST 2016
```

##### REST API
```shell
curl https://mainnet.cloudwallet.io/transactions/YOUR_TRANSACTION_ID \
    -H "apiKey: $YOUR_API_TOKEN_KEY"
```
> 결과값은 JSON으로 반환됩니다.
```json
{
 "transaction_hash": "8c14f0db3df150123e6f3dbbf30f8b955a8249b62ac1d1ff16284aefa3d06d87",
 "block_hash": [
 {
 "block_hash": "000000000003ba27aa200b1cecaad478d2b00432346c3f1f3986da1afd33e506",
 "block_height": 100000
 }
 ],
 "coinbase": true,
 "inputs": [
 {
 "transaction_hash": "0000000000000000000000000000000000000000000000000000000000000000",
 "output_index": -1,
 "address": [
 ],
 "value": ""
 }
 ],
 "outputs": [
 {
 "index": 0,
 "address": [
 "1HWqMzw1jfpXb3xyuUZ4uWXY4tqL2cW47J"
 ],
 "value": "5000000000",
 "script": "41041b0e8c2567c12536aa13357b79a073dc4444acb83c4ec7a0e2f99dd7457516c5817242da796924ca4e99947d087fedf9ce467cb9f7c6287078f801df276fdf84ac",
 "used": false
 }
 ],
 "time": "2010-12-29T11:57:43Z",
 "broadcast_time": "0001-01-01T00:00:00Z",
 "addresses": [
 "1HWqMzw1jfpXb3xyuUZ4uWXY4tqL2cW47J"
 ]
}
```

##### Node.js
```javascript
coinstackclient.getTransaction("YOUR_TRANSACTION_ID", function(err, result) {
    console.log('result', result);
});
```

##### Meteor
```javascript
// server
var result = coinstackclient.getTransactionSync("YOUR_TRANSACTION_ID");
console.log(result.transaction_hash, result.inputs);

// client
coinstackclient.getTransaction("YOUR_TRANSACTION_ID", function(err, result) {
    console.log('result', result);
});
```


# Coinstack Stamping
블록체인은 그 불가역성(한번 기록되면 삭제되거나 변경되지 않음)을 활용하여 여러 분야에 적용할 수 있습니다. 특히 문서 진위 확인 서비스(Document Stamping)에서 이 블록체인 기술이 각광을 받고 있습니다. Coinstack은 앞서 Transactions에서 설명한 바와 같이 Data output을 활용한 트랜잭션을 생성할 수 있습니다. 본인이 직접 데이터 트랜잭션을 생성하여 문서의 지문값을 등록할 경우 다음과 같은 일련의 작업들을 수행해야 합니다.

1. 본인의 비트코인 주소 생성
2. 주소에 수수료로 쓰일 비트코인 전송
3. 문서의 Hash값 추출(ex. SHA256)
4. 문서의 Hash값을 추가한 data output 생성
5. data output을 추가한 트랜잭션 생성
6. 트랜잭션을 블록체인 네트워크에 Broadcasting
7. 본인 주소의 Balance를 확인하여 수수료가 충분한지 계속 모니터링

하지만 단순히 문서의 지문값(Hash value)만을 빠르고 간단히 블록체인에 등록하고자 할 때는 이 모든 작업이 부담이 될 수 있습니다. Coinstack은 트랜잭션 level에서 데이터를 등록하는 기능을 추상화하여 위 작업을 간편하게 수행할 수 있는 Document Stamping 기능을 제공합니다.



## 1. Stamp document
SHA-256 해시를 블록체인에 기록하도록 요청합니다. 차후 요청 결과를 확인할 수 있는 stamp ID를 반환합니다. 단순히 문자열을 Coinstack에 전달하는 것만으로 번거로운 하위 작업들을 대신 처리하고 문서의 지문값을 블록체인에 등록합니다.

```shell
curl https://mainnet.cloudwallet.io/stamps \
    -H "apiKey: YOUR_API_TOKEN_KEY" \
    -H "Content-Type: application/json" \
    -X POST \
    -d '{"hash" : "YOUR_SHA256_VALUE"}' \
    -v
```

```java
String message = "Hello, world";
Sha256Hash hash = Sha256Hash.create(message.getBytes());
String stampid = coinStackClient.stampDocument(Hex.encodeHexString(hash.getBytes()));
System.out.println(stampid);
```

> 결과값은 JSON으로 리턴됩니다.
```json
{ "stampid": "7f902baec17633d12fb70892698157f595682910c96e3ee44cbdc3e2545d6665-2" }
```

## 2. Get stamp status
Stamp 요청 상태를 확인합니다. Stamp가 저장된 transaction hash와 output, confirmation 상태를 반환합니다.

```shell
curl https://mainnet.cloudwallet.io/stamps/STAMP_ID \
    -H "apiKey: YOUR_API_TOKEN_KEY"
```

```java
Stamp stamp = coinStackClient.getStamp("YOUR_STAMP_ID_VALUE");
```
> 결과값은 JSON으로 리턴됩니다.

```json
{
    "tx": "a22c7e42459b91eb325378da4721f53026962b03cd73187549c2e62cb1064464",
    "vout": 0,
    "confirmations": 522,
    "timestamp": "2015-10-30T08:43:42Z"
}
```

# Coinstack Multisig

## 1. 개요

비트코인 계정은 공개 키 암호 방식으로 관리되므로 안전하고 편리하지만, 개인키를 분실하는 경우 해당 주소에 가지고 있는 비트코인을 사용할 수 없게 된다는 위험성이 있습니다. 실제로 상당량의 비트코인이 개인키의 분실로 인해 특정 주소에서 더이상 사용되지 못하고 잠들어 있을 것으로 추정되고 있습니다. 또한 허술한 관리로 인해 개인키가 유출되는 경우 비트코인을 쉽게 도난당할 수 있습니다.

만약 비트코인의 특정 주소에 여러 개의 키를 설정할 수 있고, 그 중 몇 개의 키가 있어야 해당 주소의 비트코인을 사용할 수 있게 한다면 단 한 개뿐인 개인키의 분실이나 유출만으로 비트코인을 사용할 수 없게 되거나 도난당하는 문제를 완화할 수 있습니다.

예를 들어 2개의 키를 설정하고 그 중 하나만으로도 특정 주소의 비트코인을 사용할 수 있게 해 두면, 사용자는 2개의 키를 각각 다른 공간에 보관할 수 있습니다. 만약 한 개의 키를 잃어버리더라도 별도로 보관하고 있던 다른 키를 사용하여 해당 비트코인을 사용할 수 있게 됩니다.

또 다른 활용은 3개의 키를 설정하고 그 중 두 개가 있어야 비트코인을 사용할 수 있게 하는 방식입니다. 3개의 키 중 하나는 거래소나 지갑 같은 서비스 회사에서 관리하게 하면 사용자는 항상 두 개의 키를 준비해야 하는 불편 없이 서비스를 이용할 수 있습니다. 하지만 평소 사용하던 키를 분실하더라도 별도로 보관하던 키를 이용해 비트코인을 안전하게 옮길 수 있고, 서비스 회사 입장에서는 키가 하나밖에 없기 때문에 사용자를 속일 수 없으며, 해킹을 당하더라도 사용자의 지갑을 안전하게 보호할 수 있습니다.

이처럼 특정 비트코인 주소에 n개(n>1)의 개인 키를 설정해 두고, 해당 주소의 비트코인을 사용할 때는 그 중 m개(n>=m>=1)의 개인 키가 필요하도록 설정하는 방식을 Multisig라고 합니다.

## 2. Multisig Funding

1. P2SH 주소 생성
2. P2SH 주소에 Bitcoin 전송하는 Transaction 생성

Multisig 주소에 Bitcoin을 전송하기 위해서는 우선 Multisig 주소를 생성해야 합니다. 일반적으로 많이 쓰이는 Multisig 주소라는 용어는 사실 P2SH 주소입니다. P2SH 의 Pubkey script는 일반적으로 다음 형태를 가집니다.

```Pubkey script: OP_HASH160 <Hash160(redeemScript)> OP_EQUAL```

위 Pubkey script의 input으로 사용되는 redeemScript는 다음과 같은 구성 요소를 가집니다.

```Pubkey script: <m> <A pubkey> [B pubkey] [C pubkey...] <n> OP_CHECKMULTISIG```

m of n multisig의 경우에 n개의 pubkey와 이 트랜잭션을 사용하기 위해 필수적으로 제출해야 하는 signature 수 m을 input으로 가지는 것을 알 수 있습니다.

다음은 Coinstack에서 제공하는 Multisig 주소 관련 함수입니다.


```java
//Redeemscript 생성
	public void testRedeemScript() throws UnsupportedEncodingException, DecoderException {
		String publickey1 = "04a93d29a957d3e0064f6fde7a6c296e1ab2643f877d98ca5f52bdb9df43d3f70dfc040ee212b9bae63b2cf266ca677d31d72db53d1e928851d131d1cb9d9bde25";
		String publickey2 = "04c30518fb1d5f0e96ba9f262f31260c8ce02e322494436e4ccb0981ba687a1a2626ab30911ddad22023bcbdac1329fc73e999ff2836c608e9c8e405f4e6c0b615";
		String publickey3 = "04486a7c8754177b488982dcb13bd37fa6005a439dce55101c35ba3c5f60928036920e72a59429f0425967b735a1c52c9d2018a5ef4f63b8a8473ded4a29d5bad0";
		List<byte[]> pubkeys = new ArrayList<byte[]>(3);

		pubkeys.add(Hex.decodeHex(publickey1.toCharArray()));
		pubkeys.add(Hex.decodeHex(publickey2.toCharArray()));
		pubkeys.add(Hex.decodeHex(publickey3.toCharArray()));

		String redeemScript = MultiSig.createRedeemScript(2, pubkeys);
	}
```
public key 3개와 이 UTXO를 사용하기 위해 2개의 pubkey를 제출해야 하는 redeemscript를 생성하는 예시입니다.

```java
    //  P2SH 주소 생성
	public static String createAddressFromRedeemScript(String redeemScript, boolean isMainNet) throws MalformedInputException {
		Script redeem = null;
		String from = null;
		try {
			redeem = new Script(Hex.decodeHex(redeemScript.toCharArray()));
		} catch (ScriptException e) {
			throw new MalformedInputException("Invalid redeem script", "Parsing redeem script failed");
		} catch (DecoderException e) {
			throw new MalformedInputException("Invalid redeem script", "Parsing redeem script failed");
		}
		from = createAddressFromRedeemScript(redeem, isMainNet);
		return from.toString();
	}
```

Redeemscript를 이용하여 P2SH 주소를 생성하는 함수입니다.

최종적으로 생성되는 P2SH 주소는 일반적으로 사용되는 P2PKH 주소와 동일하게 사용할 수 있습니다. 즉, P2SH 주소에 Bitcoin을 전송하면 Multisig Transaction을 생성한 것이고 이 주소를 계속적으로 Funding을 하거나 Spending을 하면서 사용할 수 있습니다. Funding은 단순히 이 주소에 Bitcoin을 보내기만 하면 되고 Spending은 m개(m of n Multisig)의 Private key로 서명한 signature script가 제출되어야만 가능합니다.


## 3. Multisig Spending

위 장에서 Funding한 Multisig transaction UTXO를 사용하기 위해서는 m개의 Private key가 필요합니다.
m개의 private key를 array list로 전달하고 수신 주소를 입력하면 Multisig spending transaction을 생성할 수 있습니다.

```java
    @Test
	public void testMultiSigTransaction() throws Exception {
		String privateKey1 = "5Jh46BhaJJeiW8C9tTz6ockGEgYnLYfrJmGnwYdcDpBbAvWvCbv";
		// String privateKey2 = "5KF55BbKeZZqmAmpQAovn7KoBRjVdW4UN9uPGZoK1y9RrkPhnhA";
		String privateKey3 = "5JSad8KB82c3XW69e1hz8g1YFFts4GTdjHuWHkh4d4A8MZWw12N";
		String redeemScript = "52410468806910b7a3589f40c09092d3a45c64f1ef950e23d4b5aa92ad4c3de7804ed95f0f50aca9ae928fb6e00223fad667693bf3e2b716dd6c9d474ad79f5b7a107e410494bae4aa9a4c2ca6103899098ca0867f62ca24af02fee2d6473a698d92fbc8c449aa2e236c0684ebb9e0fbb23d847d4624fd8ca4a1fdc940df432c6e312e18e84104cbc882d221f567005ea61aa45d5414f25371472f6ab5973e13a39a9edc26359b6980aa4f6f34cea62e82bbe13adc7fde9fc26bba2be2e7c5f8011a68bea39bae53ae";

		List<String> prikeys = new ArrayList<String>();
		prikeys.add(privateKey1);
		prikeys.add(privateKey3);

		String to = "1F444Loh6KzUQ8u8mAsz5upBtQ356vN95s";
		long amount = Math.convertToSatoshi("0.0039");
		long fee = Math.convertToSatoshi("0.0001");

		TransactionBuilder builder = new TransactionBuilder();
		builder.addOutput(to, amount);
		builder.setFee(fee);

		String signedTx = coinStackClient.createMultiSigTransaction(builder, prikeys, redeemScript);

		assertNotNull(signedTx);

		coinStackClient.sendTransaction(signedTx);
	}
```


## 4. Multisig Partial Sign

위에 설명한 Multisig Spending은 실제 use case에는 그 쓰임이 한정적일 수 밖에 없습니다. 설명에서 보듯이 제출하는 Private key들을 모두 알고 있어야 하고 그것을 이용하여 한번에 sign을 해야만 Spending이 가능하기 때문입니다. 하지만, 실제 사용에서는 개인들 간에, 또는 기관과 개인, machine과 개인 등등이 각각 private key를 나눠 갖고 자신이 가진 private key를 자신만이 보관하고 유지해야 하는 경우가 대부분입니다. 따라서, 자신이 가지고 있는 private key로 sign을 추가하고 incomplete한 transaction을 또 다른 개인에게 전달할 수 있는 기능이 필요합니다. 이것이 Multisig partial sign입니다.

```java
	@Test
	public void testPartialSignTransaction() throws Exception {
		String privateKey3 = "5JiqywVBWDphZbR2UWnUtj3yTX52LmGhnBy8gGED7GDdxzPuRaZ";
		String privateKey2 = "5J4ZadEdMs3zaqTutP1eQoKnCGKSYrUgkPwBZrk3hNmFiz7B6Ke";
		//String privateKey1 = "5JKhaPecauUSKKZTJ2R8zhNZqFxLSDu3Q5dPU3ijSqkf2WGVekn";
		String redeemScript = "524104162a5b6239e12d3d52f2c880555934525dbb014dae7165380f77dcbf58b121b8033f59a1f7a4dcea589fc4405ac756542dfa393d53f7a559038f59b8d1084de541046a8fca1041f6ecf55aaa4e431b6c4ee72b51492330e777f2967697eb633e277eabf5d6e2ab3132b218a2d03b013ac90a80a4a2b5a27d1fa2a78cccad64d43b6f4104e850211b270fe7c97335411fcb774f6c7af0a8dd2e3360ba577e0c2979c51a375f5c256e2c8701d1b9777c15b7fc8b42af435977fe338e4a4e19683c884ad0fd53ae";
		String to = "1F444Loh6KzUQ8u8mAsz5upBtQ356vN95s";
		long amount = Math.convertToSatoshi("0.0001");
		long fee = Math.convertToSatoshi("0.0001");

		TransactionBuilder builder = new TransactionBuilder();
		builder.addOutput(to, amount);
		builder.setFee(fee);

		String signedTx = coinStackClient.createMultiSigTransactionWithPartialSign(builder, privateKey3, redeemScript);

		signedTx = coinStackClient.signMultiSigTransaction(signedTx, privateKey2, redeemScript);

		assertNotNull(signedTx);

		coinStackClient.sendTransaction(signedTx);
	}
```

위 예제는 2 of 3 Multisig spending transaction을 partial sign을 통해 생성하는 예제입니다. createMultiSigTransactionWithPartialSign 메소드는 bitcoin 수량, 수수료, 수신자 주소와 같은 Transaction 생성을 위한 기본적인 정보를 활용하여 incomplete transaction을 만들어 냅니다. 실제로 이 메소드를 활용한 결과값 transaction을 broadcast할 수 없습니다. 왜냐하면 최소 요구 signature 수량이 2인데 privateKey3을 활용하여 1개의 signature만을 추가한 transaction이기 때문입니다.  이 임시 Transaction을 완료하기 위해서는 1개의 signature를 추가해야 하는데 이때 사용하는 메소드가  signMultiSigTransaction입니다. 만약 signature 최소 요구 수량을 만족하면 완성된 transaction이 return되고 여전히 부족하면 1개의 signature가 추가된 incomplete transaction이 return됩니다.

## 5. Assets Multisig Sign

Open Assets Protocol 또한 Multisig Spending 의 적용이 가능합니다. Assets 의 소유자와 Assets 를 관리하는 시스템이 서로 권한을 나눠 운영 할 수 있게 됩니다. 1 of 2 Multisig 를 사용하면 운영 시스템에서의 Assets 에 대한 관리가 가능하며 Assets spending transaction 에 대한 서명 주체 또한 명확하게 구분됩니다. 아래 예제와 같은 방법으로 Assets 에 Multisig 를 적용할 수 있습니다.

```java
@Test
	public void testTransferAssetMultisig() throws Exception {
		String privateKey1 = "5JpxHsHWAFDJmGhQTypRf62s4UQYf59a2BnzDD2P7YxhXywraNF";
		String privateKey2 = "5JMH6KfwymcUXo1FgLJS2hC8NwicLWwTMVdHkiEgkwzvuymbast";
		String redeemScript = "5241047f78d43ff2db4c6b5e46bbc7ee6a9a253b1e66c2aece8422bd1412c6997acd8cf8561dc5ff3dbaa8a4edd94cd270204db833e0db3d6ee53276736b30184fe57741049de5e58cb6f2a3e059958a69934618b6d02277926b2d3fcf5ad8b387ffc54119ff3bd8369c893455ff7f06e1304252ccd19efb3198f1f8feb94710173123fcdd4104bc61f87c32d0262c81a0fe95979fc54133c1c97f1da7882e586728fa0e7743863841a0a09e02ac1e13ac022e0d66ac84a42aae90c9ef96866e663234dceab49353ae";

		List<String> prikeys = new ArrayList<String>();

		prikeys.add(privateKey1);
		prikeys.add(privateKey2);

		String assetID = "AKdiAtZGMuEUnZXm9tA3TPN6YJHzUarxRn";
		long assetAmount = 114;
		String to = "akDQMunRtK15fVpBR83Jnv4ezQLtyerTJTB";
		long fee = Math.convertToSatoshi("0.0001");
		String rawTx = coloringEngine.transferMultisigAsset(prikeys, redeemScript, assetID, assetAmount, to, fee);
		assertNotNull(rawTx);
		System.out.println(rawTx);
		assertNotNull(TransactionUtil.getTransactionHash(rawTx));
		sgpClient.sendTransaction(rawTx);
	}
```

# Coinstack Open Assets

비트코인은 블록체인을 활용한 대표적인 어플리케이션입니다. 블록체인의 공공 장부의 성격을 활용하여 비트코인은 현재까지 가장 안전한 전자 화폐로서 자리매김하고 있습니다. 하지만 블록체인 상에서 비트코인만을 거래할 수 있는 것은 아닙니다. Open Assets 프로토콜은 블록체인 상에서 자신만의 화폐나 어떠한 종류의 자산이라도 발행하고 유통시킬 수 있도록 해줍니다. 예를 들어 지역 화폐, 주식, 회사의 쿠폰이나 바우쳐 등을 발행할 수 있습니다. Open Assets은 비트코인 프로토콜 위에 Layer를 두어 구현되어 있기 때문에 비트코인의 안전성을 그대로 상속 받습니다. 비트코인이 해결한 double spending(이중 지출)의 문제를 Open Assets 또한 가지고 있지 않기 때문에 자산이 이중으로 발행되거나 유통하는 과정에서 자산이 감소하거나 증가하는 문제 또한 없습니다.

## 1. Open Assets 프로토콜

Open Assets은 비트코인 프로토콜을 사용하여 고객이 맞춤형 자산을 발행하거나 유통시킬 수 있습니다. 일반적인 Open Assets 트랜잭션은 비트코인 트랜잭션과 그 모습은 같지만 비트코인 트랜잭션의 output 중 Open Assets만이 가지고 있는 특수한 output, 즉, Marker output을 가집니다.다시 말하면 비트코인 트랜잭션 중에 Marker output을 가진 트랜잭션이 Open Assets 트랜잭션입니다. Marker output은 사실 OP_RETURN으로 시작하는 일반적인 비트코인 data output입니다. 다만 Marker output은 자신만이 가진 고유 ID값으로 시작하게 됩니다(OAP Marker, 0x4f41). 따라서 Marker output은 다음과 같은 데이터로 시작하게 됩니다.

OP_RETURN PUSHDATA OAP_MARKER

여기에 버젼 정보(0x0100)와 Asset count(현재 트랜잭션에서 몇 개의 asset output이 있는지), Asset 수량 리스트, 그리고 메타데이터(Asset에 관한 부가 정보)가 붙게 됩니다.

| 필드 | 설명 | 크기 |
| -- | -- | -- |
| OAP Marker | 이 트랜잭션이 Open Asset이라는 표시, 0x4f41 | 2 bytes |
| version number | Open Asset 프로토콜 버젼 번호, 0x0100 | 2 bytes |
| Asset quantity count | asset quantity list 필드의 아이템 갯수를 var-integer로 표현 | 1-9 bytes |
| Asset quantity list | 모든 output에 대응하는 Asset 수량을 LEB128 인코딩하여 표현, 0보다 큰 정수의 리스트이며 순서는 output의 순서와 일대일로 대응(Marker output은 제외) | 가변적 |
| Metadata length | Metadata 필드의 길이를 var-integer로 인코딩 | 1-9 bytes |
| Metadata | 현재의 트랜잭션과 관련한 Meta 데이터 | 가변적 |

## 2. Open Assets 주소와 Asset ID

자신의 Open Asset을 생성하기 위해서는 우선 개인키 생성이 필요합니다. 이 개인키에서 유추한 공개키를 인코딩하면 비트코인 주소가 되는데 Open Asset만의 주소 인코딩 규칙으로 인코딩을 하면 Open Asset 주소가 생성됩니다. 따라서 하나의 개인키에서 비트코인 주소와 Open Asset주소, 이렇게 두 개의 주소가 유추가 됩니다. 코인스택에서는 다음과 같은 주소 생성 기능을 제공합니다.

```java
public static String deriveAssetAddressFromPrivateKey(String privateKey)
```

생성한 개인키를 넣으면 해당하는 Open Asset 주소를 리턴받을 수 있습니다. 또한 비트코인 주소를 넣으면 해당하는 Open Asset 주소를 얻을 수 있습니다.

```java
public static String deriveAssetAddressFromBitcoinAddress(String bitcoinAddress)
```

반대로 Open Asset 주소를 넣었을 때 비트코인 주소를 리턴하는 메소드는 다음과 같습니다.

```java
public static String deriveBitcoinAddressFromAssetAddress(String assetAddress)
```

Open Assets은 자신이 원하는 어떠한 형태의 자산도 발행할 수 있습니다. 발행한 Asset을 구별하기 위해서는 Asset ID가 필요한데, Asset ID는 일반적으로 발행 Transaction의 첫 번째 input의 script를 hash해서 만들어 내고 그 첫 글자는 'A'로 시작하게 됩니다.

```RIPEMD160(SHA256(첫번째 input script))```

코인스택에서 제공하는 Asset ID 생성 함수는 다음과 같습니다.

```java
public static String createAssetID(byte[] inputScript)
```

## 3. Open Assets Issuance(발행)

다음은 코인스택에서 제공하는 Open Assets 발행 메소드입니다.

```java
	@Test
	public void testIssueAsset() throws Exception {
		String privateKeyWIF = "KyJwJ3Na9fsgvoW2v4rVGRJ7Cnb2pG4yyQQvrGWvkpuovvMRE9Kb";

		long assetAmount = 666;
		String to = "akE2cSu1JuzpXNABPXSrwkWtgL4fiTNq1xz";
		long fee = Math.convertToSatoshi("0.0002");
		String rawTx = coloringEngine.issueAsset(privateKeyWIF, assetAmount, to, fee);

		assertNotNull(rawTx);

		coinStackClient.sendTransaction(rawTx);
	}
```

Open Assets 발행을 위해서 input으로 발행 주소가 유추된 개인키, Asset 수량, Open Asset 수령 주소, 비트코인 수수료를 입력합니다. 여기서 사용하는 개인키는 Open Assets의 발행 주소가 유추되었던 원본 개인키를 사용해야 하며 추가 발행을 할 때에는 오직 이 개인키만을 사용해야 합니다.

## 4. Open Assets Transfer(전송)

다음은 코인스택에서 제공하는 Open Asset 전송 메소드입니다.

```java
	@Test
	public void testTransferAsset() throws Exception {
		String privateKeyWIF = "KztgqWTCKS6dxuUnKcJnyiHtUqJ78k91P8Rn9oNrFLhgnRh3wiiE";

		String assetID = "AKJFoih7ioqPXAHgnDzJvHE8x2FMcFerfv";
		long assetAmount = 30;
		String to = "akFNUeHPC59mrBw3E57bRjgKTdUZeMxeLur";
		long fee = Math.convertToSatoshi("0.0002");
		String rawTx = coloringEngine.transferAsset(privateKeyWIF, assetID, assetAmount, to, fee);

		assertNotNull(rawTx);

		coinStackClient.sendTransaction(rawTx);
	}
```

Open Asset 전송 메소드는 transferAsset 메소드입니다. Input 파라미터로 Asset 소유자의 개인키, Asset ID, 전송하고자 하는 Asset의 수량, 수신자의 Asset 주소, 비트코인 수수료를 넣습니다. 이때 사용하는 개인키는 Asset 소유자의 주소입니다. 따라서, 최초 발행한 Asset 발행인의 개인키일 필요는 없습니다. 앞서 말했듯이 발행인의 개인키는 Asset을 추가로 발행할 때 필요합니다. 특정 Asset 주소에 두 가지 종류 이상의 Asset이 수신되어 있을 수 있으므로 보내고자 하는 Asset ID를 반드시 명시해야 합니다.

# API Reference

코인스택 API는 모든 데이터를 객체로 취급하여 주고 받습니다. 코인스택 API를 호출할 때 파라미터로 코인스택 객체를 제공해야 하고, 또한 그 결과로 코인스택 객체를 반환받게 되므로 어떤 종류가 있는지, 어떤 속성을 갖는지 본 문서를 통해 확인하시기 바랍니다.

코인스택 API는 다음과 같이 다양한 종류의 객체를 제공하지만, 대부분 블록체인의 데이터 모델에 대응하여 설계되어 있으므로 쉽게 파악할 수 있습니다.

## BlockchainStatus

BlockchainStatus는 블록체인의 상태 정보를 나타내는 객체입니다.

| Attribute | Type | Description |
| -- | -- | -- |
| best_block_hash | string | 가장 최신의 블록체인 블록 해시 |
| best_height | number | 블록체인의 현재 높이, 즉 가장 최신의 블록 번호 |

## Block

Block은 블록체인을 구성하는 특정 블록의 상태 정보를 나타내는 객체입니다.

| Attribute | Type | Description |
| -- | -- | -- |
| block_hash | string | 블록의 해시 |
| height | number | 블록의 높이 |
| confirmation_time | <a href="https://tools.ietf.org/html/rfc3339">date</a> | 블록이 승인된 시간 |
| parent | string | 블록의 부모 블록 해시 |
| children | array[string] | 블록의 자식 블록 해시 목록 |
| transaction_list | array[string] | 요청한 블록에 포함된 트랜잭션 해시 목록 |

## Address

Address는 비트코인 주소를 의미하며, Address 관련 객체들은 아래와 같습니다.

### 1. Address Balance

Address Balance는 현재 특정 비트코인 주소에서 다른 주소로 송금 가능한 잔액을 사토시 단위로 나타내는 객체입니다.

| Attribute | Type | Description |
| -- | -- | -- |
| balance | number | 주소의 사용 가능한 잔고 (사토시 단위) |

### 2. Address History

Address History는 특정 비트코인 주소와 관련된 트랜잭션들의 해시값 목록을 가지는 객체입니다.

| Attribute | Type | Description |
| -- | -- | -- |
| (없음) | array[string] | 주소에 발생한 트랜잭션 해시 목록|

### 3. Address Unspent Outputs

Address Unspent Output은 특정 비트코인 주소의 잔액을 구성하는, 소비되지 않은 출력값에 관한 객체입니다.

| Attribute | Type | Description |
| -- | -- | -- |
| transaction_hash | string | output이 속한 트랜잭션 해시 |
| index | number | output index |
| value | string | output의 값 (사토시 단위) |
| script | string | output의 script |
| confirmations | number | output이 속한 트랜잭션이 승인된 회수 |

## Transaction

Transaction은 블록에 저장된 주소 간의 거래 정보인 트랜잭션을 나타내는 객체입니다.

| Attribute | Type | Description |
| -- | -- | -- |
| transaction_hash | string | 트랜잭션의 해시 |
| block_hash | list[string] | 트랜잭션이 포함된 블록의 해시 |
| block_hash.block_hash | string | 트랜잭션이 포함된 블록의 해시 |
| block_hash.block_height | number | 트랜잭션이 포함된 블록의 높이 |
| coinbase | boolean | Coinbase 트랜잭션 여부 |
| inputs | array[object] | Transaction Input 목록 |
| outputs | array[object] | Transaction Output 목록 |
| timestamp | string | 트랜잭션이 포함된 블록이 최초 승인된 시간 - <a href="https://tools.ietf.org/html/rfc3339">date</a> 참조 |
| initialtimestamp | string | 트랜잭션이 broadcast된 시간 - <a href="https://tools.ietf.org/html/rfc3339">date</a> 참조  |
| addresses | array[string] | 트랜잭션과 관련된 주소 목록 |

### 1. Transaction Input

Transaction Input은 특정 트랜잭션의 입력값을 나타내는 객체입니다. 하나의 트랜잭션에는 하나 이상의 입력값이 존재할 수 있으며, Transaction 객체에는 이들의 목록이 배열로 저장됩니다. Transaction Input은 이 목록을 구성하는 개별 입력값 단위에 해당합니다.

| Attribute | Type | Description |
| -- | -- | -- |
| transaction_hash | string | input이 포함된 트랜잭션  |
| output_index | number | 해당 트랜잭션에서의 output index |
| address | array[string] | 해당 input과 관련된 주소 목록 |
| value | number | input 값 (사토시 단위) |

### 2. Transaction Output

Transaction Output은 특정 트랜잭션의 출력값을 나타내는 객체입니다. 하나의 트랜잭션에는 하나 이상의 출력값이 존재할 수 있으며, Transaction 객체에는 이들의 목록이 배열로 저장됩니다. Transaction Output은 이 목록을 구성하는 개별 출력값 단위에 해당합니다.

| Attribute | Type | Description |
| -- | -- | -- |
| index | number | <!--설명이 필요합니다--> |
| address | array[string] | output과 관련된 주소 목록 |
| script | string | output의 script |
| value | number | output 값 (사토시 단위) |
| used | boolean | (Optional) output 의 사용 여부 |
| data | string | (Optional) OP_RETURN 데이터 |

## Stamp

코인스택 고유 기능인 문서 진위 확인 서비스(Document Stamping)에서 사용하는 객체입니다.

| Attribute | Type | Description |
| -- | -- | -- |
| tx | string | 스탬프 정보가 기록되어 등록된 트랜잭션 해시 |
| vout | number | 상기 트랜잭션의 출력값 중 스탬프 정보가 기록된 위치 |
| confirmations | number | 상기 트랜잭션이 속한 블럭의 승인된 횟수 |
| timestamp | string | 상기 트랜잭션이 속한 블럭이 최초 승인된 시간   - <a href="https://tools.ietf.org/html/rfc3339">date</a> 참조|

# FAQ

## Java SDK
#### 'io.blocko.coinstack.exception.MalformedInputException: Invalid private key' 오류가 발생할 때
잘못된 형식의 private key가 사용되었을 때 발생하는 오류입니다. private key를 확인하고 올바른 형식의 키를 사용해 주시기 바랍니다.


#### 'io.blocko.coinstack.exception.MalformedInputException: Invalid output'  오류가 발생할 때
기본적으로 0.00005 미만의 소량 비트코인 전송시 발생하는 오류입니다. 소량의 비트코인 전달이 꼭 필요한 경우 명시적으로 TransactionBuilder 클래스 객체의 allowDustyOutput 값을 true로 설정 후 트랜잭션을 생성하시기 바랍니다.

#### 'io.blocko.coinstack.exception.InsufficientFundException: Insufficient fund' 오류가 발생할 때
트랜잭션을 보내는 주소의 잔고가 부족할 때 발생하는 오류입니다. 남은 잔고를 확인 및 충전 후 다시 시도하시기 바랍니다. 트랜잭션을 성공적으로 보내기 위해서는 보내려는 액수에 더해 적절한 수수료를 입력해야 합니다. (최저 0.0001 BTC) 비트코인을 전송할 때뿐만 아니라 오픈어셋의 발행 및 전송, Document Stamp의 발행에도 내부적으로 트랜잭션이 발생하므로 이를 위한 수수료를 입력해야 하고, 주소에 해당 수수료가 남아 있어야 합니다.



## Bitcoin Network 일반
#### Public Bitcoin Network 잔고 충전하는 방법
먼저 코인스택 API 혹은 기타 bitcoin 지갑 서비스를 통해 나만의 주소를 생성합니다. 이후 bitcoin 거래소를 통해 bitcoin을 구매하여, 나의 주소로 이체 요청할 수 있습니다.

#### 트랜잭션이 정상적으로 처리되었는지 조회하는 방법
검증되어 블록에 포함, 전파된 트랜잭션의 경우 코인스택 API를 통해 트랜잭션을 조회하면 트랜잭션 정보에 블록 해시와 번호가 포함되어 반환됩니다. 아직 처리가 되지 않고 대기중인 트랜잭션의 경우 블록 정보가 생략되어 나옵니다.

public bitcoin에 요청한 트랜잭션의 경우, 웹사이트를 통해 블록 정보, 거래 내역등을 조회해 볼 수 있습니다. 당사에서는 https://watch.blocko.io/ 을 통해 해당 정보를 제공하고 있습니다.

대부분의 경우 블록에 포함된 트랜잭션은 불가역성이 보장되지만, 드물게 거의 동시간에 블록이 생성되어 동일 번호의 블록이 2개 이상 존재하는 경우가 있습니다. 이 경우 몇 번의 블록 생성을 거쳐 더 안정적인 블록 하나만 채택이 됩니다. 따라서 큰 금액을 거래할 경우 해당 트랜잭션이 블록에 포함되고 6개 이상의 블록 번호가 더 증가되는 것을 확인하여 트랜잭션이 안정적으로 블록체인에 기록되었음을 확인하는 편이 좋습니다.

#### 충분한 잔고를 충전했음에도 불구하고 Insufficient fund 오류가 발생할 때
충전을 위한 트랜잭션이 아직 블록에 포함되지 않았거나 안정적으로 사용하기에 충분한 블록의 번호(public bitcoin network의 경우 최소 2번)가 확보되지 않았을 경우 해당 오류가 발생합니다. 트랜잭션이 블록에 포함된 후, 혹은 충분한 번호가 확보된 후 다시 시도하시기 바랍니다. public bitcoin의 경우 블록 번호가 하나 증가하는데 평균적으로 10분이 소요됩니다.

#### Public Bitcoin Network에서 정상적인 트랜잭션이 블록에 포함되지 않을 때
사용자가 트랜잭션을 bitcoin network에 전송하면 무조건 바로 다음 블록에 포함되지는 않습니다. 트랜잭션은 일단 각 서버의 메모리풀이라는 공간에 임시로 저장됩니다. 이후 주기적으로 마이닝이라는 블럭 생성 과정을 통해 메모리 풀에 있는 트랜잭션 중 일부가 선별되어 블록이 됩니다. 이 때 트랜잭션이 선별되는 기준은 마이닝하는 서버의 정책에 따라 다르지만 일반적으로 높은 수수료를 지불하는 트랜잭션을 우선적으로 채택하게 됩니다. 따라서 최저 수수료는 KB당 0.00001 BTC 이지만, 이 보다 높은 수수료를 지불할수록 트랜잭션이 블록에 빨리 포함될 가능성이 높아집니다.

#### 개인키를 잃어버렸을 때, 주소에 남아 있던 잔고를 되찾을 수 없나요?
개인키를 잃어버렸을 경우 해당 주소에 있는 잔고를 되찾을 방법은 없습니다. 따라서 개인키를 잃어버리지 않도록 각별히 유의하시기 바랍니다. 이 외에 Multisig 기능을 활용하면 서명에 참여한 개인키의 일부를 유실했어도 나머지 키로 잔고를 찾을 수 있습니다. 자세한 내용은 7. Coinstack Multisig 장을 참고 바랍니다.

## 기타
#### 동시에 여러 트랜잭션을 보낼 때의 유의 사항
비트코인에서 사용자의 잔고는 아직 사용되지 않은 output들(utxo)을 수집 및 합산하여 표현됩니다. 각 utxo는 모두 별개의 트랜잭션에 포함되어 있기 때문에 블록체인의 여기저기에 흩어져 있을 수 있습니다. 코인스택의 transaction builder는 트랜잭션을 생성할 때 해당 주소와 연관된 utxo들을 사용자 대신 자동으로 할당해 줍니다. 따라서 동시에, 혹은 다음 utxo가 반환되기 전에 트랜잭션을 여러 번 생성하면 utxo가 중복 사용되는 경우가 발생할 수 있습니다. 그러므로 여러 개의 트랜잭션을 생성해야 하는 경우 트랜잭션 생성 간 충분한 시간 간격을 두거나, 트랜잭션 전송이 실패하는 경우 일정 시간이 지난 후 재시도해야 합니다.
