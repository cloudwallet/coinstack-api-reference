require 'rouge'

module Rouge
  module Lexers
    class Meteor < Rouge::Lexers::Javascript
      tag 'meteor'
    end
  end
end
